# hometask-10

## Ответьте на вопросы

#### 1. Как можно сменить контекст вызова? Перечислите все способы и их отличия друг от друга.
> Ответ:
Стрелочная функция — если заменить функцию на стрелочную, то она возьмёт контекст из окружения, где была создана.
Bind — напрямую фиксирует контекст и функцию, указывая что в данном случае является «this».
Call/Apply — вызывает функцию с указанным в аргементе значением «this». 
#### 2. Что такое стрелочная функция?
> Ответ:
Стрелочная функция — это короткая форма записи функции. Но не все функции можно записать таким образом. Эта функция должна быть анонимной и значит больше нигде (по имени) не вызываться. В неё можно убрать переменные из глобальной области видимости.
#### 3. Приведите свой пример конструктора. 
```js
// Ответ:
function Element(name, number, weight) {
    this.name = name;
    this.number = number;
    this.weight = weight;
} 
let natrium = new Element("Na", 11, 23);
let oxygen = new Element("H", 1, 1);
let hydrogen = new Element("O", 8, 16);

let periodicTable = () => {
  console.log(natrium);
  console.log(oxygen);
  console.log(hydrogen); 
}
console.log(periodicTable())
```
#### 4. Исправьте код так, чтобы в `this` попадал нужный контекст. Исправить нужно 3мя способами, как показано в примерах урока.

```js
  const person = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(() => {
          console.log(this.name + ' says hello to everyone!');
      }, 1000)
    }
  }

  person.sayHello();
```
```js
const person = {
  name: 'Nikita',
  sayHello: function() {
    let fn = function() {
        console.log(this.name + ' says hello to everyone!');
    }.bind(this);
    setTimeout(fn, 1000)
  }
}
person.sayHello();
```
```js
// Этот вариант не кажется корректным, тк в формулировке задания
// просят исправить код так, чтобы в this попадал нужный контекст,
// а так вообще убрали this из функции
  const person = {
    name: 'Nikita',
    sayHello: function() {
      let that = this;
      setTimeout(function() {
          console.log(that.name + ' says hello to everyone!');
      }, 1000)
    }
  }

  person.sayHello();
```

## Выполните задания

* Установите зависимости `npm install`;
* Допишите функции в `task.js`;
* Проверяйте себя при помощи тестов `npm run test`;
* Создайте Merge Request с решением.
